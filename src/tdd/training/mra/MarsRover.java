package tdd.training.mra;

import java.util.List;
import java.util.ArrayList;


public class MarsRover {
	private MarsRover rover;
	private int x=0;
	private int y=0;
	private String[] directions= {"N","E","S","W"};
	private char direction=0;
	
	private Integer[][] planet;
	List<String> obstacles= new ArrayList<>();
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) {
		obstacles= planetObstacles;
		planet = new Integer[planetX][planetY];
		setObstaclesOnGrid();
		setDirection(0);
		
	}

	private void setDirection(int c) {
		direction= 0;
		
	}

	private void setObstaclesOnGrid() {
		for (String element : obstacles){
			int a=0;
			int b=0;
			
			a= Integer.parseInt(""+element.charAt(1));
			b= Integer.parseInt(""+element.charAt(3));
				
			planet[a][b]=1;
		}
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) {

			return (planet[x][y]==1) ;

	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) {
		String result="";
		
		if(commandString==null) commandString="";
		if(commandString.length()<=1) {
			switch(commandString) {
			case "": result= getStatus(); break;
			
			case "r":  
				checkDirection("r");
				result= getStatus();
				break;
			
			case "l":  
				checkDirection("l");
				result= getStatus();
				break;
				
			case "f":  
				movingFoward();
				result= getStatus();
				break;
				
			case "b":  
				movingBackwords();
				result= getStatus();
				break;
			default:
			}
		}
		else {
			for(int i=0;i<commandString.length();i++) {
				switch(commandString.charAt(i)) {
				case 'r':  
					checkDirection("r");
					result= getStatus();
					break;
				
				case 'l':  
					checkDirection("l");
					result= getStatus();
					break;
					
				case 'f':  
					movingFoward();
					result= getStatus();
					break;
					
				case 'b':  
					movingBackwords();
					result= getStatus();
					break;
				default:
				}
			}
		}
		
		
		return result;
	}
	

	private void movingBackwords() {
		switch(direction) {
		case 0:
			if(y==0) y=9; else y--; break;
			
		case 1:
			if(x==0) x=9; else x--; break;
			
		case 2: 
			if(y==9) y=0; else y++; break;
			
		case 3:
			if(x==9) x=0; else x++; break;
		default:
		}
		
	}

	private void movingFoward() {
		switch(direction) {
		case 0:
			if(y==9) y=0; else y++; break;
			
		case 1:
			if(x==9) x=0; else x++; break;
			
		case 2: 
			if(y==0) y=9; else y--; break;
			
		case 3:
			if(x==0) x=9; else x--; break;
			
		default:
		}
		
	}



	private void checkDirection(String c) {
		if(c=="r") {
			if(direction==3) {
				direction=0;
			}
			else {
			direction++;
			}
		}
		if(c=="l") {
			if(direction==0) {
				direction=3;
			}
			else {
				direction--;
			}
			
		}
	}
	
	private String getStatus() {
		return "("+x+","+y+","+directions[direction]+")";
	}

}
