package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {
	String obstacle1="(4,7)";
	String obstacle2="(2,3)";

	@Test
	public void testMarsRover() {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add(obstacle1);
		planetObstacles.add(obstacle2);
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		
		assertTrue(rover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	public void testCommandIsEmpty() {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add(obstacle1);
		planetObstacles.add(obstacle2);
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		assertEquals("(0,0,N)",rover.executeCommand(""));
	}
	
	@Test
	public void testCommandIsNull() {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add(obstacle1);
		planetObstacles.add(obstacle2);
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		assertEquals("(0,0,N)",rover.executeCommand(null));
	}
	
	@Test
	public void testCommandTurnRight() {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add(obstacle1);
		planetObstacles.add(obstacle2);
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		assertEquals("(0,0,E)",rover.executeCommand("r"));
	}
	
	@Test
	public void testCommandTurnLeft() {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add(obstacle1);
		planetObstacles.add(obstacle2);
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		assertEquals("(0,0,W)",rover.executeCommand("l"));
	}
	
	@Test
	public void testCommandMoveFoward() {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add(obstacle1);
		planetObstacles.add(obstacle2);
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		assertEquals("(0,1,N)",rover.executeCommand("f"));
	}
	
	@Test
	public void testCommandMoveBackWords() {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add(obstacle1);
		planetObstacles.add(obstacle2);
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		rover.executeCommand("f");
		
		assertEquals("(0,0,N)",rover.executeCommand("b"));
	}
	
	@Test
	public void testCommandCombinedCommand() {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add(obstacle1);
		planetObstacles.add(obstacle2);
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		assertEquals("(2,2,E)",rover.executeCommand("ffrff"));
	}

	@Test
	public void testMovementOutOfGrid() {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add(obstacle1);
		planetObstacles.add(obstacle2);
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		assertEquals("(0,9,N)",rover.executeCommand("b"));
	}
}
